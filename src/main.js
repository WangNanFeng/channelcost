import Vue from 'vue'
import App from './App'
import './assets/css/reset.less'
import {
  Button,
  Select,
  Option,
  Table,
  TableColumn,
  Message
} from 'element-ui'
// 在调用 Vue.use 前，给 Message 添加 install 方法
Message.install = function (Vue, options) {
  Vue.prototype.$message = Message
}
Vue.use(Button)
Vue.use(Select)
Vue.use(Option)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Message)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
