import {
  getJSON
} from './ajax'
const DEFAULT_BASE_URL = 'http://172.18.8.98:8080/FCChannelCostViewServlet'
// 获取通道
export const getChannel = function(data) {
  return getJSON('GET', DEFAULT_BASE_URL, data)
}
// 查询运营商包大小
export const getPackageSize = function(data) {
  return getJSON('GET', DEFAULT_BASE_URL, data)
}
// 查询运营商包类型
export const getPackageType = function(data) {
  return getJSON('GET', DEFAULT_BASE_URL, data)
}
// 获取通道数据
export const getChannelData = function(data) {
  return getJSON('GET', DEFAULT_BASE_URL, data)
}
