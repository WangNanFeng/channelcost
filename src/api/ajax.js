/**
 * @param {String} type
 * @param {String} url
 * @param {Object} data
 */
export const getJSON = function (type, url, data) {
  const promise = new Promise(function (resolve, reject) {
    const handler = function () {
      if (this.readyState !== 4) {
        return
      }
      if (this.status === 200) {
        resolve(this.response)
      } else {}
    }
    const client = new XMLHttpRequest()
    if (type === 'GET') {
      url += (url.indexOf('?') < 0 ? '?' : '&') + param(data)
    }
    client.open(type, url)
    client.onreadystatechange = handler
    client.responseType = 'json'
    client.setRequestHeader('Accept', 'application/json')
    client.send()
  })

  return promise
}

export function param(data) {
  let url = ''
  for (var k in data) {
    let value = data[k] !== undefined ? data[k] : ''
    url += '&' + k + '=' + encodeURIComponent(value)
  }
  return url ? url.substring(1) : ''
}
